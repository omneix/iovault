package org.exdm.IOVault.registration.model;

public enum AppUserRole {
    USER,
    ADMIN
}
