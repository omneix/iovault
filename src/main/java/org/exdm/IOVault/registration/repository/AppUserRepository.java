package org.exdm.IOVault.registration.repository;

import org.exdm.IOVault.registration.model.AppUser;
import org.exdm.IOVault.registration.model.AppUserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface AppUserRepository extends JpaRepository<AppUser, Long> {

    Optional<AppUser> findByEmail(String email);

    void deleteAppUsersById(Long id);

    Optional<List<AppUser>> findAppUsersByAppUserRole(AppUserRole appUserRole);
}
