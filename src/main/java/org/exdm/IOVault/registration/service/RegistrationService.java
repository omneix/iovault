package org.exdm.IOVault.registration.service;

import lombok.AllArgsConstructor;
import org.exdm.IOVault.registration.model.AppUser;
import org.exdm.IOVault.registration.model.AppUserRole;
import org.exdm.IOVault.registration.dto.RegistrationRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
@AllArgsConstructor
public class RegistrationService {

    private final AppUserService appUserService;

    public String register(RegistrationRequest request) {
        if (request.getEmail().equals("admin@admin")) {
            return appUserService.signUpUser(new AppUser(
                    request.getFirstName(),
                    request.getLastName(),
                    request.getEmail(),
                    request.getPassword(),
                    new ArrayList<>(),
                    AppUserRole.ADMIN
            ));
        }
        return appUserService.signUpUser(new AppUser(
                request.getFirstName(),
                request.getLastName(),
                request.getEmail(),
                request.getPassword(),
                new ArrayList<>(),
                AppUserRole.USER
        ));
    }
}
