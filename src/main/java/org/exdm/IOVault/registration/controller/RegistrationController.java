package org.exdm.IOVault.registration.controller;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.exdm.IOVault.registration.dto.RegistrationRequest;
import org.exdm.IOVault.registration.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@Controller
@AllArgsConstructor
@RequiredArgsConstructor
public class RegistrationController {
    @Autowired
    private RegistrationService registrationService;

    @GetMapping("/login")
    public String viewLoginPage() {
        return "login";
    }

    @GetMapping("/registration")
    public String showRegistrationForm(Model model) {
        RegistrationRequest registrationRequest = new RegistrationRequest();
        model.addAttribute("regRequest", registrationRequest);
        return "registration";
    }

    @PostMapping("/registration")
    public String saveAppUser(@ModelAttribute("regRequest") @Valid RegistrationRequest regRequest,
                                BindingResult result, Model model) {

        if (result.hasErrors()) {
            model.addAttribute("regRequest", regRequest);
            return "registration";
        }
        registrationService.register(regRequest);
        return "success";
    }

    @PostMapping(path = "api/registration")
    @ResponseBody
    public String register(@RequestBody RegistrationRequest request) {
        return registrationService.register(request);
    }
}
