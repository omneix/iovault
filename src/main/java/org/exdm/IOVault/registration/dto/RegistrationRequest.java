package org.exdm.IOVault.registration.dto;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RegistrationRequest {

    @NotNull
    @NotEmpty(message = "User name cannot be empty")
    @Size(min = 3, max = 20)
    private String firstName;
    @NotNull
    @NotEmpty(message = "Last name cannot be empty")
    @Size(min = 3, max = 20)
    private String lastName;
    @NotNull
    @NotEmpty(message = "Email cannot be empty")
    @Size(min = 3, max = 20)
    private String email;
    @NotNull
    @NotEmpty(message = "Password cannot be empty")
    @Size(min = 3, max = 20, message = "Password length must be more than 3 symbols.")
    private String password;

}
