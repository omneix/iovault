package org.exdm.IOVault.app.controller;

import lombok.RequiredArgsConstructor;
import org.exdm.IOVault.record.model.Record;
import org.exdm.IOVault.record.service.RecordService;

import org.exdm.IOVault.registration.model.AppUser;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequiredArgsConstructor
public class UserController {

    private final RecordService recordService;

    @GetMapping("/app")
    public String getAllRecords(Model model, @AuthenticationPrincipal AppUser appUser) {
        model.addAttribute("records", recordService.findAllByAppUserId(appUser.getId()));
        return "app";
    }

    @GetMapping("/app/record/add")
    public String addRecord(Model model) {
        Record record = new Record();
        model.addAttribute("record", record);
        return "add_record";
    }

    @PostMapping("/app/record/add")
    public String saveRecord(@ModelAttribute("record") Record record, @AuthenticationPrincipal AppUser appUser,
                             BindingResult result, Model model) {
        recordService.addRecord(record, appUser.getId());
        return "redirect:/app";
    }

    @GetMapping("/app/record/update/{id}")
    public String updatRecord(@PathVariable("id") Long id, Model model) {
        model.addAttribute("record", recordService.findRecordById(id));
        return "update_record";
    }

    @PostMapping("/app/record/update/{id}")
    public String updateRecord(@ModelAttribute("record") Record record, @AuthenticationPrincipal AppUser appUser,
                             BindingResult result, Model model) {
        recordService.updateRecord(record, appUser.getId());
        return "redirect:/app";
    }

    @GetMapping("/app/record/delete/{id}")
    public String updateRecord(@PathVariable("id") Long id) {
        recordService.deleteRecord(id);
        return "redirect:/app";
    }

}
