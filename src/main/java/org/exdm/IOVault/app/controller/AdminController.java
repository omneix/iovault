package org.exdm.IOVault.app.controller;

import lombok.RequiredArgsConstructor;
import org.exdm.IOVault.record.service.RecordService;
import org.exdm.IOVault.registration.model.AppUser;
import org.exdm.IOVault.registration.model.AppUserRole;
import org.exdm.IOVault.registration.service.AppUserService;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
@RequiredArgsConstructor
public class AdminController {

    private final AppUserService appUserService;

    @GetMapping("/admin/users")
    public String getAllRecords(Model model) {
        model.addAttribute("users", appUserService.findAppUsersByAppUserRole(AppUserRole.USER));
        return "users";
    }

    @GetMapping("/admin/users/delete/{id}")
    public String deleteUser(@PathVariable("id") Long id) {
        appUserService.deleteAppUserById(id);
        return "redirect:/admin/users";
    }

}
