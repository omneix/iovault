package org.exdm.IOVault.record.controller.api;

import lombok.RequiredArgsConstructor;
import org.exdm.IOVault.record.model.Record;
import org.exdm.IOVault.record.service.RecordService;
import org.exdm.IOVault.registration.model.AppUser;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/record")
public class RecordController {

    private final RecordService recordService;

    @GetMapping("/all")
    public ResponseEntity<List<Record>> getAllRecords(@AuthenticationPrincipal AppUser appUser) {
        List<Record> records = recordService.findAllByAppUserId(appUser.getId());
        return new ResponseEntity<>(records, HttpStatus.OK);
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<Record> getRecordById(@PathVariable("id") Long id, @AuthenticationPrincipal AppUser appUser) {
        Record record = recordService.findRecordIdAndAppUserId(id, appUser.getId());
        return new ResponseEntity<>(record, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<Record> addRecord(@RequestBody Record record, @AuthenticationPrincipal AppUser appUser) {
        Record newRecord = recordService.addRecord(record, appUser.getId());
        return new ResponseEntity<>(newRecord, HttpStatus.CREATED);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Record> updateRecord(@PathVariable("id") Long id, @RequestBody Record record, @AuthenticationPrincipal AppUser appUser) {
        record.setId(id);
        Record updateRecord = recordService.updateRecord(record, appUser.getId());
        return new ResponseEntity<>(updateRecord, HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteRecord(@PathVariable("id") Long id,  @AuthenticationPrincipal AppUser appUser) {
        recordService.deleteRecordByIdAndAppUserId(id, appUser.getId());
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
