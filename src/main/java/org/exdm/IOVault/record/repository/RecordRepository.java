package org.exdm.IOVault.record.repository;

import org.exdm.IOVault.record.model.Record;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RecordRepository extends JpaRepository<Record, Long> {

    void deleteRecordById(Long id);

    void deleteRecordByIdAndAppUserId(Long recordId, Long appUserId);

    Optional<Record> findRecordById(Long id);

    Record findRecordByIdAndAppUserId(Long recordId, Long appUserId);

    List<Record> findAllByAppUserId(Long appUserId);
}
