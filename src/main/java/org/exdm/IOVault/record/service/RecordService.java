package org.exdm.IOVault.record.service;

import org.exdm.IOVault.record.model.Record;

import java.util.List;

public interface RecordService {

    Record addRecord(Record record, Long appUserId);

    List<Record> findAllByAppUserId(Long appUserId);
    Record updateRecord(Record record, Long appUserId);
    Record findRecordById(Long id);

   void deleteRecord(Long id);

    Record findRecordIdAndAppUserId(Long recordId, Long appUserId);

    void deleteRecordByIdAndAppUserId(Long recordId, Long appUserId);

}
