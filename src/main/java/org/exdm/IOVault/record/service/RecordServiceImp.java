package org.exdm.IOVault.record.service;

import lombok.RequiredArgsConstructor;
import org.exdm.IOVault.record.model.Record;
import org.exdm.IOVault.record.repository.RecordRepository;
import org.exdm.IOVault.registration.model.AppUser;
import org.exdm.IOVault.registration.repository.AppUserRepository;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class RecordServiceImp implements RecordService {

    private final RecordRepository recordRepository;
    private final AppUserRepository appUserRepository;

    public Record addRecord(Record record, Long appUserId) {
        record.setCode(UUID.randomUUID().toString());
        AppUser appUser = appUserRepository.findById(appUserId).orElseThrow();
        record.setAppUser(appUser);
        return recordRepository.save(record);
    }

    public List<Record> findAllRecords() {
        return recordRepository.findAll();
    }

    public List<Record> findAllByAppUserId(Long appUserId) {
        return recordRepository.findAllByAppUserId(appUserId);
    }

    public Record updateRecord(Record record, Long appUserId) {
        record.setCode(UUID.randomUUID().toString());
        AppUser appUser = appUserRepository.findById(appUserId).orElseThrow();
        record.setAppUser(appUser);
        return recordRepository.save(record);
    }

    public Record findRecordById(Long id) {
        return recordRepository.findRecordById(id).orElseThrow(() -> new UsernameNotFoundException(
                "Record by " + id + " was not found"));
    }

    public Record findRecordIdAndAppUserId(Long recordId, Long appUserId) {
        return recordRepository.findRecordByIdAndAppUserId(recordId, appUserId);
    }

    @Transactional
    public void deleteRecord(Long id) {
        recordRepository.deleteRecordById(id);
    }

    @Transactional
    public void deleteRecordByIdAndAppUserId(Long recordId, Long appUserId) {
        recordRepository.deleteRecordByIdAndAppUserId(recordId, appUserId);
    }

}
