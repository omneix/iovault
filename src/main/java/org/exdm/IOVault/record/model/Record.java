package org.exdm.IOVault.record.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.exdm.IOVault.registration.model.AppUser;
import org.exdm.IOVault.security.AttributeEncryptor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Record {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String code;
    private int type = 1;
    @NotNull
    @NotEmpty(message = "Name of record cannot be empty")
    private String name;
    private String notes;
    @NotNull
    @NotEmpty(message = "User name cannot be empty")
    private String username;
    @NotNull
    @NotEmpty(message = "Password cannot be empty")
    @Convert(converter = AttributeEncryptor.class)
    private String password;
    private String uri;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "app_user_id")
    private AppUser appUser;

}
