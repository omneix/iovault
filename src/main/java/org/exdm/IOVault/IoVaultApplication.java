package org.exdm.IOVault;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class IoVaultApplication {

	public static void main(String[] args) {

		SpringApplication.run(IoVaultApplication.class, args);
	}

}
