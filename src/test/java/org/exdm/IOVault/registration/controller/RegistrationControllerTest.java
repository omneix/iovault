package org.exdm.IOVault.registration.controller;

import org.exdm.IOVault.registration.dto.RegistrationRequest;
import org.exdm.IOVault.registration.repository.AppUserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class RegistrationControllerTest {

    @Autowired
    private RegistrationController registrationController;

    @Autowired
    private AppUserRepository appUserRepository;

    @Test
    void register() {
        RegistrationRequest regReq1 = RegistrationRequest.builder().firstName("Agent").lastName("Smith").email("ex@mail.com").password("pass").build();
        RegistrationRequest regReq2 = RegistrationRequest.builder().firstName("User").lastName("Bob").email("exdm@mail.com").password("asdf").build();
        registrationController.register(regReq1);
        registrationController.register(regReq2);

        assertNotNull(appUserRepository.findByEmail("ex@mail.com").orElseThrow());
        assertNotNull(appUserRepository.findByEmail("exdm@mail.com").orElseThrow());
    }
}
