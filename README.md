# IOVault

## Description

Basic web-application for storing/retrieving user credentials and passwords.
Powered by Java, Spring, Postgres and Thymeleaf templates.

![Main_UI](img/Screenshot.png)
